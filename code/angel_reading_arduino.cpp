#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
 
#define BNO055_SAMPLERATE_DELAY_MS (100)
 
Adafruit_BNO055 myIMU = Adafruit_BNO055();
 
void setup() {
	Serial.begin(115200);
	myIMU.begin();
	delay(1000);
	int8_t temp=myIMU.getTemp();
	myIMU.setExtCrystalUse(true);
}
 
void loop() {
	imu::Vector<3> mag =myIMU.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER); 
	Serial.print("X-value:");
	Serial.print(mag.x());
	Serial.print(", Y-value:");
	Serial.print(mag.y());
	Serial.print(", Z-value:");
	Serial.println(mag.z());
 
delay(1000);
 
}